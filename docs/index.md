# CamilLAN

This is the setproject for Camille's LAN parties.


# Quick Install

If you have access to an existing Camillan bundle like this :

![share](images/share_bundle.png)


From a terminal :
```
cd /tmp
cat camillan-bundle-* | tar xv -C ~/src
cd ~/src/camillan
./scripts/install_bootstrap
./scripts/install_games
```


# Install 

If your `cache` folder is empty, you have to download the games files :
```
./scripts/download
```

To install everything (need a sudo access) :
```
./scripts/install_bootstrap
./scripts/install_games
```

# Deploy



# Quickstart

Start pegasus launcher with :
```
./start
```


# flatpack

[offline install](https://unix.stackexchange.com/questions/404905/offline-install-of-a-flatpak-application)
[flatpack create-usb](https://docs.flatpak.org/fr/latest/flatpak-command-reference.html#flatpak-create-usb)


# TODO

- [ ] toblo
- ( ] liquidwar
- [ ] flatpack support
- [ ] appimage support
- [ ] backup/restore resolution
- [ ] store game settings


# Changelog

version 0.5, 2020-02-27 :
* delete games dir before install

version 0.4, 2020-02-27 :
* path fix in dekstop file

version 0.3, 2020-02-27 :
* desktop file fixed
* bundle content /games to /cache

version 0.2, 2020-02-27 :
* bundle deploy
* bundle creation with stats
* xdg desktop menu added
* simple logo created
* game stuntrally
* snap support

version 0.1, 2020-02-27 :
* game bombsquad
* game urban terror
* game netpanzer
* game teeworlds
* game openarena
